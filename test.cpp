//
// Created by cosimo on 24/03/18.
//

#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "Equalizer.h"
#include "Image.h"
#include "omp.h"
#include <vector>
#include <algorithm>

using namespace std;
using namespace cv;




double computeAverage(double* data,int size) {
    double average = 0;
    for (int i = 0; i < size; i++) {
        average += data[i];
    }
    average = average / ((double) size);
    return average;
}
double computeMedian(double *data, int size){

    sort(data, data+size);

    if(size%2==1)//la size è dispari
        return data[size/2 +1];

    else
        return data [size/2];
}

void writeToFile(ofstream *file, double * times,int size, double average, double median){
    if (file->is_open()){
        *file<<"Execution times: ";
        for(int p=0; p<size; p++){
            *file <<times[p]<< " ";
        }
        *file<<endl;
        *file<<endl;

        *file<<"Average : "<<average<<endl;
        *file<<"Median : "<<median<<endl;
        *file<<endl;
        *file<<endl;


    }
}

int main(void) {


    Equalizer equalizer;
    int nIterations = 10;
    float gamma = 3;
    ofstream fileLin(
            "../testDataLinear.txt");
    ofstream fileLog(
            "../testDataLogarithmic.txt");
    ofstream fileExp(
            "../testDataGamma.txt");
    //Image Img("/home/leonardo/Scrivania/test_img/lena.jpg", true);

    string pathSmallImage= "../lenna400x300.jpg";
    string pathMediumImage = "../SH1000x1000.jpg";
    string pathBigImage = "../lenna1960x1960.jpg";
    string pathHugeImage = "../img6984x2832.jpg";
    int nThreads[6] = {1, 2, 4, 8, 16, 32};

    int nImages = 4;
    string pathImages[nImages];
    pathImages[0]=pathSmallImage;
    pathImages[1]=pathMediumImage;
    pathImages[2]=pathBigImage;
    pathImages[3]=pathHugeImage;
    double logOneThread = 0;
    double expOneThread = 0;
    double linOneThread = 0;
    for (int k=0;k<nImages ; k++) {
        Image Img(pathImages[k], true);

        if (fileLin.is_open()) {
            fileLin << endl << "Working on  " << pathImages[k]<< endl;
        }
        if (fileLog.is_open()) {
            fileLog << endl << "Working on  " << pathImages[k]<< endl;
        }
        if (fileExp.is_open()) {
            fileExp << endl << "Working on  " << pathImages[k]<< endl;

            for (int i = 1; i < 6; i++) {

                cout << "NThread = " << nThreads[i] << endl;

                omp_set_num_threads(nThreads[i]);
                if (fileLin.is_open()) {
                    fileLin << endl << "Results with " << nThreads[i] << " threads " << endl;
                }
                if (fileLog.is_open()) {
                    fileLog << endl << "Results with " << nThreads[i] << " threads " << endl;
                }
                if (fileExp.is_open()) {
                    fileExp << endl << "Results with " << nThreads[i] << " threads " << endl;
                }
                double *linearEqualizationTimes = new double[nIterations];
                double *logTransformationTimes = new double[nIterations];
                double *expTransformationTimes = new double[nIterations];
                double linAverage, logAverage, expAverage;
                double linMedian, logMedian, expMedian;

                for (int j = 0; j < nIterations; j++) {
                    equalizer.histogramEqualization(&Img);
                    linearEqualizationTimes[j] = equalizer.getLinEqExecutionTime();

                    equalizer.logTransform(&Img);
                    logTransformationTimes[j] = equalizer.getLogTrasfExecutionTime();

                    equalizer.gammaTransform(&Img, gamma);
                    expTransformationTimes[j] = equalizer.getExpTrasfExecutionTime();

                }
                linAverage = computeAverage(linearEqualizationTimes, nIterations);
                linMedian = computeMedian(linearEqualizationTimes, nIterations);

                logAverage = computeAverage(logTransformationTimes, nIterations);
                logMedian = computeMedian(logTransformationTimes, nIterations);

                expAverage = computeAverage(expTransformationTimes, nIterations);
                expMedian = computeMedian(expTransformationTimes, nIterations);

                fileLin << "Linear Equalization : " << endl;
                writeToFile(&fileLin, linearEqualizationTimes, nIterations, linAverage, linMedian);


                fileLog << "Logarithmic Transform : " << endl;
                writeToFile(&fileLog, logTransformationTimes, nIterations, logAverage, logMedian);

                fileExp << "Gamma Transform : " << endl;
                writeToFile(&fileExp, expTransformationTimes, nIterations, expAverage, expMedian);

                fileLin.close();
                fileLog.close();
                fileExp.close();

            }

        }
    }
    fileLin.close();
    fileLog.close();
    fileExp.close();
    return 0;
}

