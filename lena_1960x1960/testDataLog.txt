
Results with 1 threads 
Logarithmic Transform : 
Execution times: 0.437917 0.43799 0.438806 0.43884 0.439174 0.444262 0.445874 0.445904 0.445983 0.464156 

Average : 0.443891
Median : 0.444262


SpeedUp : 1

Results with 2 threads 
Logarithmic Transform : 
Execution times: 0.235968 0.236111 0.236178 0.236242 0.236261 0.236283 0.236408 0.236485 0.236689 0.236977 

Average : 0.23636
Median : 0.236283


SpeedUp : 1.88021

Results with 4 threads 
Logarithmic Transform : 
Execution times: 0.139067 0.139128 0.139224 0.139282 0.139356 0.13952 0.139615 0.139832 0.140666 0.148117 

Average : 0.140381
Median : 0.13952


SpeedUp : 3.18422

Results with 8 threads 
Logarithmic Transform : 
Execution times: 0.143219 0.143289 0.144246 0.145226 0.145578 0.145744 0.145978 0.146788 0.147342 0.158385 

Average : 0.14658
Median : 0.145744


SpeedUp : 3.04823

Results with 16 threads 
Logarithmic Transform : 
Execution times: 0.142296 0.14284 0.145398 0.146501 0.147293 0.147921 0.148718 0.149472 0.153258 0.153476 

Average : 0.147717
Median : 0.147921


SpeedUp : 3.00337

Results with 32 threads 
Logarithmic Transform : 
Execution times: 0.146309 0.147517 0.147826 0.148152 0.148182 0.151845 0.152183 0.152331 0.15393 0.158481 

Average : 0.150676
Median : 0.151845


SpeedUp : 2.92575
