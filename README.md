# Multithreading Image Processing Techniques with OpenMP

This repository implements 3 of the main Image processing operations: Histogram Equalization, Logarithmic Transform and Gamma Tranform with OpenMP.

### Prerequisites

To run this project is sufficient to have:

* GCC 4.4.x or later
* pkg-config
* [OpenCV](https://opencv.org/releases.html) 3.x or later

## Getting Started

To create an Image "Object" use the constructor that take in input a path to an image ( *.png, *.jpg, *.jpeg, *.bmp ... ) and a boolean: "true" if you want to read the BGR version of the image or 2false2 if you want to read the grayscale one. 


To perform one of the Image processing operations is necessary to create an "Equalizer" Object and then launch one of the following methods:

* histogramEqualization(Image *img): takes in input an Image and return a new Image Object obtained from the Histogram Equalization of the input
* logTransform(Image* img): takes in input an Image and return a new Image Object obtained from the Logarithic Transformation of the input
* gammaTransform(Image* img,float gamma): takes in input an Image and a value for the gamma parameter, return a new Image Object obtained from the Gamma Transformation of the input

Utilities: 
* Image::saveImg(String path): save the Image object 
* Image::showImg(): show the Image object

## Running the tests

In this repository is present a test on four images ( already present in the repository ):

* Small image: 400x300 px
* Medium image: 1000x1000 px
* Big image: 1960x1960 px
* Huge image: 6984x2832 px

The test run all the Image operations with 1, 2, 4, 8, 16, 32 threads and save the results on a csv.

## Authors

* **Leonardo Scommegna** 
* **Cosimo Rulli**