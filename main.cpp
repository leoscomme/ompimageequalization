#include <iostream>
#include <opencv2/core/core.hpp>


#include "Equalizer.h"


using namespace std;
using namespace cv;


int main(void) {
    Image myImg1("/home/leonardo/Scrivania/lena.png", true);

    Equalizer equalizer;
    Image equalizedLenna= equalizer.logTransform(&myImg1);
    equalizedLenna.showImg();

    equalizedLenna = equalizer.gammaTransform(&myImg1,3);
    equalizedLenna.showImg();

    equalizedLenna = equalizer.histogramEqualization(&myImg1);
    equalizedLenna.showImg();

    return 0;

}
