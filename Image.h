//
// Created by leonardo on 20/03/18.
//

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

#ifndef OPENMPEQUALIZATION_IMAGE_H
#define OPENMPEQUALIZATION_IMAGE_H

using namespace std;

class Image {
private:
    vector<cv::Mat> channels;
    vector <int*> histogram;
    vector<int*> cumulativeHistogram;
    vector<unsigned char*> dataChannels;
    int width,height;
    bool BGR;
public:
    Image(string path, bool BGR);
    Image(cv::Mat* image,bool BGR);
    Image(vector<unsigned char*> channels,int height, int width);
    ~Image();

    const vector<int*> *getHistogram();
    int* computeSinglePlaneHistogram(int index);

    int* computeSinglePlaneHistogramOld(int index);

    const vector <int*> *getCumulativeHistogram();
    int * getSinglePlaneCumulativeHistogram(int index);

    void saveImg(string path);
    void showImg();
    void showHist(string path);

    const vector<unsigned char*>* getChannels();
    const int getWidth();
    const int getHeight();
    const bool isBGR(){return BGR;}
};



#endif //OPENMPEQUALIZATION_IMAGE_H
