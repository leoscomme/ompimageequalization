import numpy as np
import matplotlib.pyplot as plt
import sys

def main():

    path= sys.argv[1]
    histograms= np.loadtxt(path, dtype=np.float, ndmin=2)
    new_path=path[:-4]

    labels=['blue', 'green', 'red']
    if histograms.shape[0]==1:
        plt.plot(histograms[0])
        plt.title("greyscale")
        current_path=new_path+"greyscalehistogram.png"
        plt.savefig(current_path)
        plt.show()
    else:
        for i in range(histograms.shape[0]):
            plt.plot(histograms[i])
            plt.title(labels[i])
            current_path= new_path + labels[i]+ "histogram.png"
            plt.savefig(current_path)
            plt.show()


if __name__== "__main__":
    main()