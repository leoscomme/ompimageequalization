//
// Created by leonardo on 19/03/18.
//

#include <math.h>
#include <algorithm>
#include "Equalizer.h"
#include <chrono>



Image Equalizer::histogramEqualization(Image *img){
    const vector<unsigned char*> *channels=img->getChannels();
    vector<unsigned char*> newChannels;
    int size=img->getWidth()*img->getHeight();
    auto begin = chrono::high_resolution_clock::now();
    const vector<int *> * cumHist=img->getCumulativeHistogram();
    double res= 255/((double) size);
    for(int i=0; i<channels->size(); i++){
        unsigned char *eqChannel=new unsigned char[size];
#pragma omp parallel for
        for(int j=0; j<size; j++){
            int index=(*channels)[i][j];
            eqChannel[j]=(unsigned char)((*cumHist)[i][index]*res);
        }
        newChannels.push_back(eqChannel);

    }
    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> diff = end-begin;
    linEqExecutionTime=diff.count();
    printf("Histogram Equalization Execution Time : \%f\n",diff.count());
    return  Image(newChannels,img->getHeight(),img->getWidth());

}



Image Equalizer::logTransform(Image* img) {
    const vector<unsigned char*>* channels;
    channels = img->getChannels();
    vector<unsigned char*> newChannels;
    int size = img->getWidth()*img->getHeight();
    auto begin = std::chrono::high_resolution_clock::now();
    for(auto channel : *channels){
        unsigned char* eqChannel = new unsigned char[size];
        double c = 255/(log(1+getMax(channel,size)));
        #pragma omp parallel for
        for(int i=0; i<size; ++i){
            eqChannel[i] = (unsigned char)c*log(1+int(channel[i]));
        }
        newChannels.push_back(eqChannel);
    }
    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> diff = end-begin;
    logTrasfExecutionTime= diff.count();
    printf("Logarithmic Transformation Execution Time : \%f\n",diff.count());
    return  Image(newChannels,img->getHeight(),img->getWidth());
}

Image Equalizer::gammaTransform(Image* img,float gamma) {
    const vector<unsigned char*>* channels;
    channels = img->getChannels();
    vector<unsigned char*> newChannels;
    int size = img->getWidth()*img->getHeight();
    auto begin = chrono::high_resolution_clock::now();
    for(auto channel : *channels){
        unsigned char* eqChannel = new unsigned char[size];
        #pragma omp parallel for
        for(int i=0; i<size; ++i){
            eqChannel[i] = 255*pow((float)channel[i]/255,gamma);
        }
        newChannels.push_back(eqChannel);
    }
    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> diff = end-begin;
    expTrasfExecutionTime= diff.count();
    printf("Gamma Transformation Execution Time : \%f\n",diff.count());
    return  Image(newChannels,img->getHeight(),img->getWidth());
}


int Equalizer::getMax(unsigned char* array, int size) {
    int max = 0;
    for(int i=0; i<size; ++i){
        if((int)array[i]>max)
            max = (int)array[i];
    }
    return max;
}

double Equalizer::getLinEqExecutionTime() const {
    return linEqExecutionTime;
}

double Equalizer::getLogTrasfExecutionTime() const {
    return logTrasfExecutionTime;
}

double Equalizer::getExpTrasfExecutionTime() const {
    return expTrasfExecutionTime;
}

