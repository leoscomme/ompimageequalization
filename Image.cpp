//
// Created by leonardo on 20/03/18.
//

#include "Image.h"
#include <fstream>

using namespace std;

Image::Image(string path, bool BGR) {
    cv::Mat image;
    if(BGR==true)
        image = cv::imread(path, CV_LOAD_IMAGE_UNCHANGED);
    else
        image = cv::imread(path, CV_LOAD_IMAGE_GRAYSCALE);

    if(! image.data )
    {
        cerr <<  "Could not open or find the image" << endl ;
    }
    else{
        this->BGR = BGR;
        this->height = image.rows;
        this->width = image.cols;

        if(BGR== true) {
            cv::split(image,this->channels);
        }
        else{
            this->channels.push_back(image);
        }


    }
}

Image::Image(vector<unsigned char *> channels, int height, int width) {
    for(int i=0;i<channels.size();++i){
        cv::Mat ch(height, width, CV_8UC1, channels[i]);
        this->channels.push_back(ch.clone());
        delete[] channels[i];
    }
    if(channels.size()==3)
        this->BGR = true;
    else
        this->BGR = false;
    this->height = height;
    this->width = width;

}



Image::Image(cv::Mat *image,bool BGR) {
    this->BGR = BGR;
    this->height = (*image).rows;
    this->width = (*image).cols;

    if(BGR== true) {
        cv::split(*image,this->channels);
    }
    else{
        this->channels.push_back(*image);
    }

}

Image::~Image() {
    for(auto it=histogram.begin(); it!=histogram.end(); ++it){
        delete [](*it);
    }
    for(auto it=cumulativeHistogram.begin(); it!=cumulativeHistogram.end(); ++it){
        delete [](*it);
    }
}

const vector <int *>* Image::getHistogram() {
    if (histogram.empty()) {
        for (int i = 0; i < channels.size(); i++) {
            histogram.push_back(computeSinglePlaneHistogram(i));
        }
    }
    return &histogram;
}




int* Image::computeSinglePlaneHistogram(int index) {
    int *tempHist= new int [256];

    #pragma omp parallel for
    for(int i=0; i< 256; i++){
        tempHist[i]=0;
    }

    cv::Mat channel = channels[index];
    #pragma omp parallel for reduction(+:tempHist[:255])
    for( int i=0; i<height*width; ++i){
        int pos = (int)channel.data[i];
        tempHist[pos]++;
    }


    return tempHist;

}


const vector <int*> *Image::getCumulativeHistogram(){
    if(cumulativeHistogram.empty()){
        for(int i=0; i<channels.size(); i++){
            cumulativeHistogram.push_back(getSinglePlaneCumulativeHistogram(i));
        }

    }
    return &cumulativeHistogram;
}


int * Image::getSinglePlaneCumulativeHistogram(int index) {
    int * tempCumHist=new int [256];
    const vector<int*>* hist = getHistogram();
    tempCumHist[0]=(*hist)[index][0];
    for(int i=1; i<256; i++){
        tempCumHist[i]=tempCumHist[i-1]+ (*hist)[index][i];
    }

    return tempCumHist;
}

const vector<unsigned char*>* Image::getChannels() {
    if(dataChannels.empty()) {
        for (auto channel = this->channels.begin(); channel != this->channels.end(); ++channel) {

            dataChannels.push_back(channel->data);
        }
    }
    return &dataChannels;
}

void Image::saveImg(string path) {
    cv::Mat finalImg;
    cv::merge(this->channels,finalImg);
    cv::imwrite(path,finalImg);
}

void Image::showImg() {
    cv::Mat finalImg;
    cv::merge(this->channels,finalImg);

    cv::namedWindow( "Image", cv::WINDOW_NORMAL );// Create a window for display.
    cv::resizeWindow("Image", 600,600);
    cv::imshow( "Image", finalImg );

    cv::waitKey(0);
}


void Image::showHist(string path){
    getHistogram();
    ofstream file(path);
    if(file.is_open()){
        for(auto it=histogram.begin(); it!=histogram.end(); ++it){
            int *channel= *it;
            for(int i=0; i<256; i++){
                file<<channel[i]<<" ";
            }
            file<<endl;
        }
        file.close();

    }
    string pythonPath = "/usr/bin/anaconda2/bin/python2.7"; //insert your python path
    string command=pythonPath + " ../printHistogram.py ";

    system((command+ path).c_str());

}





const int Image::getHeight() {
    return this->height;
}

const int Image::getWidth() {
    return this->width;
}

